## Install
1. Use Ctrl+Alt+T open terminal and run command to install the ley:
```
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
```

2. Then add the apt repository via command:
```
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
```

3. Finaly check updates and install *sublime-text* via running the following commands:
```
sudo apt-get update
sudo apt-get install sublime-text
```

## Uninstall
```
sudo apt-get remove sublime-text && sudo apt-get autoremove
```
The official Sublime Text apt repository can be removed by going to *System Setting* -> *Software & Updates* -> *Other Software tab*