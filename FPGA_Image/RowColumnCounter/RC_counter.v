module RC_counter
(
	input rst_n,
	input vsync,
	input hsync,
	input dvalid,
	input[DW-1:0] din,
	input clk
	);

parameter IW = 640,
				 IH = 512,
				 DW = 8,
				 IW_DW = 12,
				 IH_DW = 12;

				 reg[IH_DW-1:0] line_counter;
				 reg[IH_DW-1:0] column_counter;

				 reg rst_all;

endmodule
