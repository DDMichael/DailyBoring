`timescale 1 ps/ 1ps
module sin_lut_tb;
	parameter DW = 15;
	parameter AW = 10;
	parameter ADDR_MAX = 900;

	parameter const_half_pi = ADDR_MAX-1; //Address for 90 degrees
	parameter const_pi = ADDR_MAX*2-1;	//Address for 180 degrees
	parameter const_double_pi = ADDR_MAX*4-1; // Address for 360 degrees

	reg [AW+2-1:0] address_tmp;
	reg[AW-1:0] address;
	reg clock;
	wire [DW-1:0] q_tmp;
	reg [DW+1-1:0] q_tmp1;
	wire [DW-1:0] q;

	initial
	begin
		clock <= 1'b0;
		address_tmp <= {AW+2{1'b0}};
	end

	sin_lut u0(
				.clock(clock),
				.address(address),
				.q(q_tmp)
		);

	always @(clock)
		clock <= #50 ~clock;

	always @(posedge clock)
		if(address_tmp == const_double_pi)
		begin
			address_tmp <= {AW+2{1'b0}};
			address <= {AW{1'b0}};
		end
		else
		begin
				address_tmp <= address_tmp + 1'b1;
				if(address_tmp <= const_half_pi)
					address <= address_tmp[AW-1:0];
				else if(address_tmp <= const_pi)
					address <= const_pi-address_tmp;
				else if(address_tmp <= (const_pi+const_half_pi))
					address <= address_tmp-const_pi;
				else
					address <= const_double_pi-address_tmp;
		end

		always@(posedge clock)
			if(address_tmp <= const_pi)
				q_tmp1 <= {1'b0, q_tmp};
			else
				q_tmp1 <= {DW+1{1'b0}} - {1'b0, q_tmp};

		assign q = q_tmp1[DW-1:0];

endmodule