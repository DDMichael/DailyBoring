clear all
close all
clc
t = [0:0.1:90]; % Input range 0-90 degrees with step 0.1 degree
x = pi * t/180
sin_val = sin(x); % extract sine array
fid = fopen('sine.mif','wt');
fprintf(fid, 'width = 14; \n'); % Converted data width is 14 bit
fprintf(fid, 'depth = 1023; \n'); % In total 900 points, deep is at least 1024
fprintf(fid, 'address_radix = uns; \n'); %Address is not labeled
fprintf(fid, 'data_radix = dec; \n'); % Data is decimal
fprintf(fid, 'content begin\n');
for j = 1:901
	i = j - 1;
	k = round(sin_val(j) * 16384);
	if(k == 16384)
		k = 16383;
	end
	fprintf(fid,  '%d:%d;\n', i, k );
end
fprintf(fid, 'end;\n');
fclose(fid)