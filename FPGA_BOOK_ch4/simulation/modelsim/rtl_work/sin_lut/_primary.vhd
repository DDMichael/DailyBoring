library verilog;
use verilog.vl_types.all;
entity sin_lut is
    generic(
        DW              : integer := 15;
        AW              : integer := 10;
        DEPTH           : integer := 1024
    );
    port(
        address         : in     vl_logic;
        clock           : in     vl_logic;
        q               : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DW : constant is 1;
    attribute mti_svvh_generic_type of AW : constant is 1;
    attribute mti_svvh_generic_type of DEPTH : constant is 1;
end sin_lut;
