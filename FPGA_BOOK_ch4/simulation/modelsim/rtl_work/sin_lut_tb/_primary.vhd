library verilog;
use verilog.vl_types.all;
entity sin_lut_tb is
    generic(
        DW              : integer := 15;
        AW              : integer := 10;
        ADDR_MAX        : integer := 900;
        const_half_pi   : vl_notype;
        const_pi        : vl_notype;
        const_double_pi : vl_notype
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DW : constant is 1;
    attribute mti_svvh_generic_type of AW : constant is 1;
    attribute mti_svvh_generic_type of ADDR_MAX : constant is 1;
    attribute mti_svvh_generic_type of const_half_pi : constant is 3;
    attribute mti_svvh_generic_type of const_pi : constant is 3;
    attribute mti_svvh_generic_type of const_double_pi : constant is 3;
end sin_lut_tb;
