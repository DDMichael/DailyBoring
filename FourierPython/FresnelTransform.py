import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


_lambda = 0.6*10**(-6) # wavelength, unit: m
delta = 10*_lambda # sampling perit, unit: m

z = 0.07 # propagation distance, unit: m
M = 512 # space size

c = np.arange(1, M+1, dtype = int)
r = np.arange(1, M+1, dtype = int)
C, R = np.meshgrid(c, r)
THOR=(np.sqrt(np.square(R-M/2-1)+np.square(C-M/2-1)))*delta;  
OB = (THOR <= 5*10**(-4)) # Circular aperture of radius R, a logical operation
QP = np.exp(1j* math.pi/_lambda/z*np.square(THOR))
print(QP)
FD = np.fft.fftshift(np.fft.fft2(np.fft.fftshift(np.multiply(OB,QP))))
FD = np.abs(FD)
fig = plt.figure()
ax = fig.gca(projection = '3d')
surf = ax.plot_surface(C, R, FD, linewidth = 0, antialiased = True, cmap = cm.viridis)
plt.show()
