# from scipy import fftpack, ndimage
from PIL import Image
from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt

image = ndimage.imread('text.png', flatten = True)

fft2 = np.fft.fftshift(np.fft.fft2(image))
remmax = lambda x: x/x.max()
remmin = lambda x: x - np.amin(x, axis=(0,1), keepdims = True)
touint8 = lambda x: (remmax(remmin(x))*(256-1e-4)).astype(int)
fft2 = touint8(fft2)

# ifft2 = np.fft.ifft2(fft2)
plt.imshow(abs(fft2))

plt.show()