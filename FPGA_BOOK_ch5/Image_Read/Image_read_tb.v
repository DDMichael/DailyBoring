`timescale 1 ns/ 1 ns

module Image_read_tb;
	
	parameter iw = 200,     // Image width
					 ih = 200,		// Image height
					 dvd_dw = 8, // Data bitwidth

					// Video timing parameters
					 h_total = 760,
					 v_total = 288,
					 sync_b = 5,
					 sync_e = 55,
					 vld_b = 65;

	reg clk, reset_l;
	// pixel_total = h_total*v_total = 218880
	// pixel_total = 60*h_total*v_total = 13132800
	// Generate clock approximate 13.1328 MHz
	always @(reset_l or clk)
	begin
		if((~(reset_l)) == 1'b1)
			clk <= 1'b0;
		else 
			clk <= #9645 (~(clk));
	end

	wire dv_clk;
	wire dvsyn;
	wire dhsyn;
	wire [dvd_dw-1:0] dvd;

	Image_src #(iw, ih, dvd_dw, h_total, v_total, sync_b, sync_e, vld_b)
	img_src_ins
	(
		.clk(clk),
		.reset_l(reset_l),
		//.src_sel(src_sel),
		.test_data(dvd),
		.test_dvalid(dhsyn),
		.test_vsync(dvsyn),
		.clk_out(dv_clk)
		);

endmodule