/*
**********************************************************************************************
**      Input file      : None 
**      Component name  : image_src.v
**      Author          : ZhengXiaoliang
**      Company         : WHUT
**      Description     : to simulate dvd stream 
**********************************************************************************************
*/
`timescale 1 ns/1 ns

`define SEEK_SET 0
`define SEEK_CUR 1
`define SEEK_END 2

module  Image_src(
		reset_l,
		clk,
	//	src_sel,
		test_vsync,
		test_dvalid,
		test_data,
		clk_out
	);

  parameter iw = 200*3;//640*3 3 bytes per pixels
  parameter ih = 200;//512 plus one command line
  parameter dw = 8;   
  
  parameter h_total = 760;
  parameter v_total = 288;
  
  parameter sync_b = 5;
  parameter sync_e = 55;
  parameter vld_b  = 65;
   
  input reset_l,clk;
//  input [3:0]src_sel;//to select the input file  
  output test_vsync,test_dvalid,clk_out;
  output [dw-1:0]test_data;
  
  reg [dw-1:0]test_data_reg;
  reg test_vsync_temp;
  reg test_dvalid_tmp;
  reg [1:0]test_dvalid_r;
  
  reg [10:0]       h_cnt;
  reg [10:0]       v_cnt;
  
  integer fp_r;
  integer cnt=0;
	
	assign clk_out = clk;//output the dv clk
	
	assign test_data = test_data_reg;//test data output 
	
	//read data from file

	always @(posedge clk or  posedge test_vsync_temp )
	
	if (((~(test_vsync_temp))) == 1'b0)
		cnt<=0;//clear file pointer when a new frame comes
	else 
	begin
		if (test_dvalid_tmp == 1'b1) 
		begin
			fp_r = $fopen("lena.txt", "r"); 
			$fseek(fp_r,cnt,0); // Find the location of current file
			$fscanf(fp_r, "%02x\n", test_data_reg); // Store the data into test_data_reg follow the required format
			cnt <= cnt + 4 ; // Move to the folowing data
			$fclose(fp_r);
			$display("%02x",test_data_reg);  //for debug use
		end
	end

//horizon counter
	always @(posedge clk or posedge reset_l)
  if (((~(reset_l))) == 1'b1)
	 h_cnt <= #1 {11{1'b0}};
  else 
  begin
	 if (h_cnt == ((h_total - 1)))
		h_cnt <= #1 {11{1'b0}};
	 else
		h_cnt <= #1 h_cnt + 11'b00000000001;
  end

//vertical counter
  always @(posedge clk or posedge reset_l)
  if (((~(reset_l))) == 1'b1)
	 v_cnt <= #1 {11{1'b0}};
  else 
  begin
	 if (h_cnt == ((h_total - 1)))
	 begin
		if (v_cnt == ((v_total - 1)))
		   v_cnt <= #1 {11{1'b0}};
		else
		   v_cnt <= #1 v_cnt + 11'b00000000001;
	 end
  end

//field sync
  always   @(posedge clk or posedge reset_l)
  if (((~(reset_l))) == 1'b1)
	 test_vsync_temp <= #1 1'b1;
  else 
  begin
	 if (v_cnt >= sync_b & v_cnt <= sync_e)
		test_vsync_temp <= #1 1'b1;
	 else
		test_vsync_temp <= #1 1'b0;
  end

  assign test_vsync = (~test_vsync_temp);

//horizon sync
  always @(posedge clk or posedge reset_l)
  if (((~(reset_l))) == 1'b1)
	 test_dvalid_tmp <= #1 1'b0;
  else 
  begin
	 if (v_cnt >= vld_b & v_cnt < ((vld_b + ih)))
	 begin
		if (h_cnt == 10'b0000000000)
		   test_dvalid_tmp <= #1 1'b1;
		else if (h_cnt == iw)
		   test_dvalid_tmp <= #1 1'b0;
	 end
	 else
		test_dvalid_tmp <= #1 1'b0;
  end

  assign test_dvalid = test_dvalid_tmp;

  always @(posedge clk or posedge reset_l)
  if (((~(reset_l))) == 1'b1)
	 test_dvalid_r <= #1 2'b00;
  else 
	 test_dvalid_r <= #1 ({test_dvalid_r[0], test_dvalid_tmp});

endmodule