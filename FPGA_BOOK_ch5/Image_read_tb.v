`timescale 1 ns/ 1 ns

module Image_read_tb;
	
	parameter iw = 640,     // Image width
					 ih = 512,		// Image height
					 dvd_dw = 8, // Data bitwidth

					// Video timing parameters
					 h_total = 1440,
					 v_total = 600,
					 sync_b = 5,
					 sync_e = 55,
					 vld_b = 65;

	reg clk;
	// pixel_total = h_total*v_total
	// pixel_total = 60*h_total*v_total
	// Generate clock approximate 51.84 MHz
	always @(reset_l or clk)
	begin
		if((~(reset_l)) == 1'b1)
			clk <= 1'b0;
		else 
			clk <= #9645 (~(clk));
	end

	wire dv_clk;
	wire dvsyn;
	wire dhsyn;
	wire [dvd_dw-1:0] dvd;

	image_src #()
	img_src_ins
	(
		.clk(clk),
		.reset_l(reset_l),
		.src_sel(src_sel),
		.test_data(dvd),
		.test_dvalid(dhsyn),
		.test_vsync(dvsyn),
		.clk_out(dv_clk)
		);

endmodule